<?php

namespace Internit\TesteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('InternitTesteBundle:Default:index.html.twig');
    }
}
